require 'test_helper'

class DermCodesControllerTest < ActionController::TestCase
  setup do
    @derm_code = derm_codes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:derm_codes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create derm_code" do
    assert_difference('DermCode.count') do
      post :create, derm_code: { code: @derm_code.code, description: @derm_code.description }
    end

    assert_redirected_to derm_code_path(assigns(:derm_code))
  end

  test "should show derm_code" do
    get :show, id: @derm_code
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @derm_code
    assert_response :success
  end

  test "should update derm_code" do
    patch :update, id: @derm_code, derm_code: { code: @derm_code.code, description: @derm_code.description }
    assert_redirected_to derm_code_path(assigns(:derm_code))
  end

  test "should destroy derm_code" do
    assert_difference('DermCode.count', -1) do
      delete :destroy, id: @derm_code
    end

    assert_redirected_to derm_codes_path
  end
end
