class DermCodesController < ApplicationController
  before_action :set_derm_code, only: [:show, :edit, :update, :destroy]

  # GET /derm_codes
  # GET /derm_codes.json
  def index
    @derm_codes = DermCode.all
  end

  # GET /derm_codes/1
  # GET /derm_codes/1.json
  def show
  end

  # GET /derm_codes/new
  def new
    @derm_code = DermCode.new
  end

  # GET /derm_codes/1/edit
  def edit
  end

  # POST /derm_codes
  # POST /derm_codes.json
  def create
    @derm_code = DermCode.new(derm_code_params)

    respond_to do |format|
      if @derm_code.save
        format.html { redirect_to @derm_code, notice: 'Derm code was successfully created.' }
        format.json { render :show, status: :created, location: @derm_code }
      else
        format.html { render :new }
        format.json { render json: @derm_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /derm_codes/1
  # PATCH/PUT /derm_codes/1.json
  def update
    respond_to do |format|
      if @derm_code.update(derm_code_params)
        format.html { redirect_to @derm_code, notice: 'Derm code was successfully updated.' }
        format.json { render :show, status: :ok, location: @derm_code }
      else
        format.html { render :edit }
        format.json { render json: @derm_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /derm_codes/1
  # DELETE /derm_codes/1.json
  def destroy
    @derm_code.destroy
    respond_to do |format|
      format.html { redirect_to derm_codes_url, notice: 'Derm code was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_derm_code
      @derm_code = DermCode.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def derm_code_params
      params.require(:derm_code).permit(:code, :description)
    end
end
