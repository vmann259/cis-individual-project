json.array!(@chairs) do |chair|
  json.extract! chair, :id
  json.url chair_url(chair, format: :json)
end
