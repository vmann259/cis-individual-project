json.array!(@derm_codes) do |derm_code|
  json.extract! derm_code, :id, :code, :description, :amount
  json.url derm_code_url(derm_code, format: :json)
end
