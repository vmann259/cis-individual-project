class Chair < ActiveRecord::Base

  has_many :appointments
  belongs_to :time_slot, through: :appointment

end
