class Appointment < ActiveRecord::Base

  belongs_to :physician
  belongs_to :patient
  belongs_to :derm_code
  belongs_to :time_slot
  belongs_to :chair
  belongs_to :status
end
