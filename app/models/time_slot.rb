class TimeSlot < ActiveRecord::Base

  has_many :appointments
  has_many :chairs, through: :appointments
  end
