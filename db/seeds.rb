# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#   initialcodes = DermCode.create(name: '919.0', description 'Abrasion without infection')

#   dermcodes
# = DermCode.create([{ code: '919.0', description: 'Abrasion without infection' }])

require 'csv'

DermCode.delete_all
CSV.foreach("#{Rails.root}/db/codes1.csv") do |row|
  DermCode.create!(:code => row[0], :description => row[1], :amount => row[2])
end

TimeSlot.delete_all
CSV.foreach("#{Rails.root}/db/timeslot.csv") do |row|
  TimeSlot.create!(:start_time => row[0], :end_time => row[1],)
end

Patient.delete_all
CSV.foreach("#{Rails.root}/db/patient.csv") do |row|
  Patient.create!(:name => row[0])
end

Physician.delete_all
CSV.foreach("#{Rails.root}/db/physician.csv") do |row|
  Physician.create!(:name => row[0])
end