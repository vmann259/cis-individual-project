class AddStartTimeToTimeSlot < ActiveRecord::Migration
  def change
    add_column :time_slots, :start_time, :string
  end
end
