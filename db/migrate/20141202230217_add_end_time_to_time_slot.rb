class AddEndTimeToTimeSlot < ActiveRecord::Migration
  def change
    add_column :time_slots, :end_time, :string
  end
end
