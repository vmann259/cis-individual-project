class RemoveStartTimeFromTimeSlot < ActiveRecord::Migration
  def change
    remove_column :time_slots, :start_time, :time
  end
end
