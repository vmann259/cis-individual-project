class RemoveEndTimeFromTimeSlot < ActiveRecord::Migration
  def change
    remove_column :time_slots, :end_time, :time
  end
end
