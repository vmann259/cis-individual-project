class RemoveAppointmentDateFromAppointment < ActiveRecord::Migration
  def change
    remove_column :appointments, :appointment_date, :datetime
  end
end
