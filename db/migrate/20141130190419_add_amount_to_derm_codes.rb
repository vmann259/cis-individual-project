class AddAmountToDermCodes < ActiveRecord::Migration
  def change
    add_column :derm_codes, :amount, :integer
  end
end
