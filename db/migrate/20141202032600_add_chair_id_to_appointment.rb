class AddChairIdToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :chair_id, :integer
  end
end
