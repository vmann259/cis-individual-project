class CreateDermCodes < ActiveRecord::Migration
  def change
    create_table :derm_codes do |t|
      t.string :code
      t.string :description

      t.timestamps
    end
  end
end
