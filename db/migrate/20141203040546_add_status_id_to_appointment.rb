class AddStatusIdToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :status_id, :integer
  end
end
