class AddTimeSlotIdToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :time_slot_id, :integer
  end
end
